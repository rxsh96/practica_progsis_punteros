#include <stdio.h>
#define MAX 10

static char caracteres[MAX] = {'a','b','c'};
static long enteros_largos[MAX];
static char *punteros[MAX] = {"hola","mundo","hello","world"};

void iterar_enteros(int *);
void iterar_caracteres(char *);
void iterar_enteros_largos(long *);
void iterar_punteros(char **);

int main(){
	
	static int enteros[MAX];
	enteros[0] = 100;
	enteros[1] = 300;
	enteros[5] = 50;

	enteros_largos[0] = 102939384;
	enteros_largos[1] = 123434543;
	enteros_largos[2] = 984284932;

	printf("\nIterando Enteros\n");
	iterar_enteros(enteros);
	printf("\nIterando Enteros Largos\n");
	iterar_enteros_largos(enteros_largos);
	printf("\nIterando Caracteres\n");
	iterar_caracteres(caracteres);
	printf("\nIterando Punteros\n");
	iterar_punteros(punteros);
}

void iterar_enteros(int *enteros){
	int *p_anterior = NULL;
	int *p;
	for(int i = 0; i < MAX; i++){
		p = enteros + i;
		printf("El valor de p es: %d\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);
		p_anterior = p;
	}
}

void iterar_caracteres(char *caracteres){
	char *p_anterior = NULL;
	char *p;
	for(int i = 0; i < MAX ; i++)	{
		p = caracteres + i;
		printf("El valor de p es: %c\n", (char)*p);
		printf("La direccion de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n", (long)p-(long)p_anterior);
		p_anterior = p;
	}
}

void iterar_enteros_largos(long *largos){
	long *p_anterior = NULL;
	long *p;
	for(int i = 0; i < MAX; i++){
		p = largos + i;
		printf("El valor de p es: %ld\n", *p);
		printf("La direccion de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n", (long)p-(long)p_anterior);
		p_anterior = p;
	}
}

void iterar_punteros(char **puntero){
	char **p_anterior = NULL;
	char **p;
	for(int i = 0; i < MAX; i++){
		p = puntero + i;
		printf("El valor de p es: %s\n", *p);
		printf("La direccion de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n", (long)p-(long)p_anterior);
		p_anterior = p;
	}
}
/*
*¿Cuál es el efecto de añadir la palabra clave static a la declaración del arreglo enteros en la línea 15 de main.c?
*La palabra clave static le asigna el valor por defecto a todas las casillas del arreglo que no estan inicializadas con un valor.
* Si el arreglo es int, inicializa con cero
* Evita que en estos lugares se almacene valores basura. 

 
Al implementar las otras funciones, el offset entre direcciones es diferente, ¿que representa en realidad el valor de este offset?
El primer valor del offset es diferente, este valor significaria la distancia que existen entre la ubicacion de memoria de la primera posición del arreglo y la anterior. Una vez alcanzada la segunda dirección del arreglo, el offset se mantiene constante en 4 bytes, debido a que este es el espacio que ocupa una variable de tipo int, 1 byte, en caso de que la variable sea char y 8 bytes en caso de que sea un arreglo de punteros o punteros a long.
*
* 
*En la línea 33 del archivo main.c, ¿por qué es necesario hacer un casting a long?
Porque hacer una resta entre punteros (sin hacer casting) resulta en otra dirección de memoria. Si se transforma estas direcciones primero a enteros, entonces si se puede hacer una resta que
tenga como resultado el offset.
* 
*/
